﻿using System;
using System.Text;

namespace _01_base
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration de variable
            // Déclaration d'une variable   type nomVariable;
            int j;

            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            j = 42;
            Console.WriteLine(j);

            // Déclaration et initialisation de variable 
            int k = 34;
            Console.WriteLine(k);

            // Déclaration multiple de variable
            double largeur, hauteur = 34.5;
            largeur = 56.7;
            Console.WriteLine(largeur + " " + hauteur); // + => concaténation
            #endregion

            #region Littéral
            // Littéral booléen
            bool tst = false; //true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 'a';
            char chrUnicode = '\u0045'; // Caractère en UTF-16
            char chrUnicodeHexa = '\x45';
            Console.WriteLine(chr + " " + chrUnicode + " " + chrUnicodeHexa);

            // Littéral entier -> int par défaut
            long l = 123L;  // L > long
            uint ui = 123U; // U -> unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> changement de base
            int dec = 123;      // décimal (base 10) par défaut
            int hexa = 0xFF2;   // 0x => héxadécimal (base 16)
            int bin = 0b010101; // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + bin + " " + hexa);

            // Littéral nombre à virgule flottante
            double d1 = 12.3;
            double d2 = .5;
            double d3 = 1.23e3;
            Console.WriteLine(d1 + " " + d2 + " " + d3);

            // Littéral nombre à virgule flottante -> par défaut double
            float f = 12.3F;        // F -> float
            decimal deci = 12.3M;   // M -> Decimal
            Console.WriteLine(f + " " + deci);
            #endregion

            #region Typage implicite
            // Type implicite -> var
            // Le type est déterminé par le type de la littérale, de l'expression ou du retour d'une méthode
            var v1 = 10.5;     // v1 -> double
            var v2 = "hello";  // v2 -> string
            var v3 = j + k;    // v3 -> int
            Console.WriteLine(v1 + " " + v2 + " " + v3);
            #endregion

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 1;
            Console.WriteLine(@while);

            #region Transtypage
            // Transtypage autormatique (pas de perte de donnée)
            // type inférieur => type supérieur
            int ti1 = 45;
            double ti2 = ti1;
            long ti3 = ti1;
            Console.WriteLine(ti1 + " " + ti2 + " " + ti3);

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 1.2;
            int te2 = (int)te1;
            float te3 = (float)te1;
            decimal te4 = (decimal)te1;

            // Dépassement de capacité
            int dep1 = 300;            // 00000000 00000000 00000001 00101100    300
            sbyte dep2 = (sbyte)dep1;  //                            00101100    44

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    dep2 = (sbyte)dep1; // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassement de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir l'option checked dans les propriétés du projet dans Advanced Build Settings
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            unchecked
            {
                dep2 = (sbyte)dep1;  // plus de vérification de dépassement de capacité
            }
            Console.WriteLine(dep1 + " " + dep2);
            #endregion

            #region Fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = Convert.ToInt32("123");
            Console.WriteLine(fc1);
            double fc2 = Convert.ToDouble("1,2");
            Console.WriteLine(fc2);

            // int fc = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException

            // Conversion d'une chaine de caractères en entier
            // Parse
            int fc3 = int.Parse("345");
            //fc3 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(fc3);

            // TryParse
            int fc4;
            bool tstCnv = int.TryParse("123", out fc4); // Retourne true et la convertion est affecté à fc4
            Console.WriteLine(fc4 + " " + tstCnv);

            tstCnv = int.TryParse("azerty", out fc4); // Retourne false, 0 est affecté à fc4
            Console.WriteLine(fc4 + " " + tstCnv);
            #endregion

            #region Type référence
            StringBuilder sb1 = new StringBuilder("azerty");
            StringBuilder sb2 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = sb1;
            Console.WriteLine(sb1 + " " + sb2);
            sb1 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = null; // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(sb1 + " " + sb2);
            #endregion

            #region Constante
            const double PI = 3.14;
            Console.WriteLine(PI);
            double pi2 = PI * 2;
            Console.WriteLine(pi2);
            // PI = 3.1419;     // Erreur: on ne peut pas modifier la valeur d'une constante
            //const int vr1;    // Erreur: on est obligé d'initialiser une constante
            #endregion

            #region Opérateur
            // opérateur arithmétique
            // division 0

            // entier
            //int zero = 0;
            //Console.WriteLine(1/zero);

            // Nombre à virgule flottante
            Console.WriteLine(1.0 / 0.0); // double.PositiveInfinity
            Console.WriteLine(-1.0 / 0.0); // double.NegativeInfinity
            Console.WriteLine(0.0 / 0.0); // double.NaN

            // Incrémentation
            // pré-incrémentation
            int inc = 0;
            int res = ++inc; // inc =1 res=1
            Console.WriteLine(res + " " + inc);

            // post-incrementation
            inc = 0;
            res = inc++; // res=0 inc=1
            Console.WriteLine(res + " " + inc);

            // Affectation composée
            res = 12;
            res += 23; // res=res+23;
            Console.WriteLine(res);

            // Opérateur de comparaison
            bool tst1 = res > 100;      // Une comparaison a pour résultat un booléen
            Console.WriteLine(tst1);    // false

            // Opérateur logique
            // Opérateur Non !
            Console.WriteLine(!tst1 + "  " + !(res > 100));  // true

            // a  b | et ou   ou exclusif
            //---------------------------   
            // f  f | f | f | f
            // v  f | f | v | v
            // f  v | f | v | v
            // v  v | v | v | f

            // Opérateur court-circuit && et ||
            // && -> dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tst2 = res > 100 && res++ == 35;
            Console.WriteLine(tst2 + " " + res);

            // || -> dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool tst3 = res == 35 || res > 100; // true
            Console.WriteLine(tst3 + " " + res);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1   
            Console.WriteLine(Convert.ToString(b & 0b1101, 2)); //          et => 1000
            Console.WriteLine(Convert.ToString(b | 0b1101, 2)); //          ou =>  11110
            Console.WriteLine(Convert.ToString(b ^ 0b1101, 2)); // ou exclusif =>  11000

            // Decalage
            Console.WriteLine(Convert.ToString(b << 1, 2));  // Décalage à gauche de 1 (insertion de un 0 à droite) => 110100
            Console.WriteLine(Convert.ToString(b << 3, 2)); // Décalage à gauche de 1 (insertion de 3 0 à droite)  => 11010000
            Console.WriteLine(Convert.ToString(b >> 2, 2)); // Décalage à droite de 2 (insertion de 2 0 à gauche)  => 110

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str5 = "azerty";
            string resStr = str5 ?? "valeur defaut";
            Console.WriteLine(resStr); // azerty

            str5 = null;
            resStr = str5 ?? "valeur defaut";
            Console.WriteLine(resStr); // valeur defaut
            #endregion

            #region Promotion numérique
            // Le type le + petit est promu vers le + grand type des deux
            int pn1 = 11;
            double pn2 = 12.3;
            double res2 = pn1 + pn2; // pn1 est promu en double => 23.3
            Console.WriteLine(res2);

            res2 = pn1 / 2; // un entier divisé par un entier donne un entier
            Console.WriteLine(res2); // 5.0

            res2 = pn1 / 2.0; // on divise par double, pn1 est promu en double => la résultat est un double
            // res2 =  ((double)pn1) / 2; // idem en convertissant avec un cast pn1 vers un double, 2 est promu en double 2.0
            Console.WriteLine(res2); // 5.5

            // sbyte, byte, short, ushort, char sont promus en int
            short sh1 = 1;
            short sh2 = 2;
            int sh3 = sh1 + sh2; // sh1 et sh2 sont promu en int
            Console.WriteLine(sh3);
            #endregion

            #region Format chaine de caractère
            int xi = 1;
            int yi = 5;

            // 1
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);  // On peut définir directement le format dans la mèthode WriteLine
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi);

            // 2
            Console.WriteLine($"xi={xi} yi={yi} xi+yi={xi + yi}");

            // 3
            Console.WriteLine("c:\tmp\newfile.txt"); // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine(@"c:\tmp\newfile.txt"); // @ devant la littérale supprime l'interprétation dans la chaine des caractères spéciaux 

            Console.WriteLine("c:\\tmp\\newfile.txt"); // on peut arriver au même resultat en doublant les antislash
            #endregion

            // Saisie au clavier
            Console.Write("Saisir une chaine de caractères ");
            string str6 = Console.ReadLine(); // -> ReadLine ne retourne que des chaines de caractère 
            Console.WriteLine(str6);
            Console.Write("Saisir un nombre entier ");
            int vv = Convert.ToInt32(Console.ReadLine()); // Si l'on veut saisir un nombre, il faut convertir la chaine en entier avec Convert.toInt32 ou int.Parse 
            Console.WriteLine(vv);

            #region Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 =  4
            Console.WriteLine("Somme: entrer 2 entiers");
            int val1 = Convert.ToInt32(Console.ReadLine());
            int val2 = Convert.ToInt32(Console.ReadLine());
            int somme = val1 + val2;
            Console.WriteLine(somme);
            Console.WriteLine($"{val1}+{val2}={somme}");
            #endregion

            #region Exercice Moyenne
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.WriteLine("Moyenne: entrer 2 entiers");
            int va1 = Convert.ToInt32(Console.ReadLine());
            int va2 = int.Parse(Console.ReadLine());
            double moyenne = (va1 + va2) / 2.0;
            Console.WriteLine($"moyenne={moyenne}");
            #endregion

            Console.ReadKey();
        }
    }
}
