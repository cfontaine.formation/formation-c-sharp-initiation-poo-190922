﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_SurchargeOperateur
{
    internal class Fraction
    {
        public int Numerateur { get; set; }
    
        public int Denominateur { get; set; }

        public Fraction(int numerateur, int denominateur)
        {
            Numerateur = numerateur;
            Denominateur = denominateur;
        }

        public double Calculer()
        {
            return ((double)Numerateur) / Denominateur;
        }

        // - unaire
        public static Fraction operator -(Fraction f)
        {
            return new Fraction(-f.Numerateur, f.Denominateur);
        }

        // *
        public static Fraction operator *(Fraction f1,Fraction f2)
        {
            return new Fraction(f1.Numerateur * f2.Numerateur, f1.Denominateur * f2.Denominateur);
        }

        public static Fraction operator *(Fraction f, int scalaire)
        {
            return new Fraction(f.Numerateur * scalaire, f.Denominateur);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            return f1.Numerateur==f2.Numerateur && f1.Denominateur==f2.Denominateur;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction fraction &&
                   Numerateur == fraction.Numerateur &&
                   Denominateur == fraction.Denominateur;
        }

        public override int GetHashCode()
        {
            int hashCode = 1421187769;
            hashCode = hashCode * -1521134295 + Numerateur.GetHashCode();
            hashCode = hashCode * -1521134295 + Denominateur.GetHashCode();
            return hashCode;
        }



        public override string ToString()
        {
            return $"{Numerateur}/{Denominateur}";
        }

    }
}
