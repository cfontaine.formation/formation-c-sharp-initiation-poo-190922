﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_SurchargeOperateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Fraction a = new Fraction(1, 2);
            Console.WriteLine(a);
            Fraction b = -a;
            Console.WriteLine(b);

            Fraction c = a * b;
            Console.WriteLine(c);

            Fraction d = a * 2;
            Console.WriteLine(d);

            b*= a; // b=b*a

            Console.WriteLine(b);
            Console.WriteLine(a == b); // false
            Console.WriteLine(a != b); // true
            Console.ReadKey();
        }
    }
}
