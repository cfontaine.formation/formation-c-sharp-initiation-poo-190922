﻿using System;

namespace _03_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension
            // Déclarer un tableau
            double[] t = new double[5];

            // Valeur d'initialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            // - type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(t[1]);
            t[0] = 1.23;
            Console.WriteLine(t[0]);

            // Si l'on essaye d'accéder à un élément en dehors du tableau -> IndexOutOfRangeException
            // Console.WriteLine(t[40]);

            // Nombre d'élément du tableau
            Console.WriteLine(t.Length);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < t.Length; i++)
            {
                Console.WriteLine(t[i]);
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double v in t) // ou
            foreach (var v in t)     // var -> v est de type double
            {
                Console.WriteLine(v);   // v uniquement en lecture
                //v = 34.5;             // => Erreur ,on ne peut pas modifier elm
            }

            // Déclarer et initaliser un tableau
            string[] tStr = { "hello", "world", "azerty", "bonjour" };
            foreach (var s in tStr)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine(tStr.Length);

            // On peut utiliser une variable entière pour définir la taille du tableau
            Console.WriteLine("Saisir la taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t2 = new int[size];
            Console.WriteLine(t2.Length);
            #endregion

            #region Exercice tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            //  int[] t ={ -7,-6,-4,-8,-3}; //1

            Console.Write("Taille du tableau=");
            int taille = Convert.ToInt32(Console.ReadLine()); //2
            int[] tab = new int[taille];
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = tab[0]; //int.MinValue;
            double somme = 0;
            for (int i = 0; i < tab.Length; i++)
            {
                if (tab[i] > max)
                {
                    max = tab[i];
                }
                somme += tab[i];
            }
            Console.WriteLine($"Maximum={max} Moyenne={somme / tab.Length}");
            #endregion

            #region Tableau Multidimensions
            // Déclaration d'un tableau à 2 dimensions
            // Nombre maximum de dimmesion 32
            int[,] tab2d = new int[3, 2];

            // Accès à un élémént d'un tableau à 2 dimensions
            tab2d[2, 0] = 42;
            Console.WriteLine(tab2d[2, 0]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length); // -> 6

            // Nombre de dimension du tableau
            Console.WriteLine(tab2d.Rank); // -> 2

            // Nombre de ligne du tableau
            Console.WriteLine(tab2d.GetLength(0)); // -> 3

            // Nombre de colonne du tableau
            Console.WriteLine(tab2d.GetLength(1)); // -> 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int l = 0; l < tab2d.GetLength(0); l++)
            {
                for (int c = 0; c < tab2d.GetLength(1); c++)
                {
                    Console.Write($"{tab2d[l, c]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var e in tab2d)
            {
                Console.WriteLine(e);
            }

            // Déclaration d'un tableau à 3 dimensions
            char[,,] tab3d = new char[3, 4, 1];

            // Déclaration et initialisation tableau à 2 dimensions
            char[,] tab2dChr = { { 'a', 'r', 'z' }, { 'c', 't', 'r' } };
            foreach (var e in tab2dChr)
            {
                Console.WriteLine(e);
            }
            #endregion

            #region Tableau en escalier
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[3][];
            tabEsc[0] = new int[4];
            tabEsc[1] = new int[2];
            tabEsc[2] = new int[3];

            // Accès à un élément
            tabEsc[1][1] = 3;
            Console.WriteLine(tabEsc[0][0]);

            Console.WriteLine(tabEsc.Length);       // Nombre de ligne

            Console.WriteLine(tabEsc[0].Length);    // Nombre d'élément de la première ligne ->4

            // Nombre de colonne par ligne
            for (int i = 0; i < tabEsc.Length; i++)
            {
                Console.WriteLine(tabEsc[i].Length);
            }

            // Parcourir complétement un tableau de tableau => for
            for (int l = 0; l < tabEsc.Length; l++)
            {
                for (int c = 0; c < tabEsc[l].Length; c++)
                {
                    Console.Write($"{tabEsc[l][c]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau de tableau => foreach
            foreach (var row in tabEsc)
            {
                foreach (var elm in row)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][]
            {
                new int[]{1,3,5},
                new int[]{1,2,6,8},
                new int[]{2,7}
            };

            foreach (var row in tabEsc2)
            {
                foreach (var elm in row)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }
            #endregion

            Console.ReadKey();
        }
    }
}
