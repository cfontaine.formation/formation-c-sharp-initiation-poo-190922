﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePolymorphime
{
    internal class Terrain
    {
        Forme[] _formes = new Forme[20];
        public int CptForme { get; set; }

        public void Ajouter(Forme f)
        {
            if (CptForme < _formes.Length)
            {
                _formes[CptForme] = f;
                CptForme++;
            }
        }

        public double Surface()
        {
            double somme = 0.0;
            for (int i = 0; i < CptForme; i++)
            {
                somme += _formes[i].Surface(); ;
            }
            return somme;
        }

        public double Surface(Couleurs couleur)
        {
            double somme = 0.0;
            for (int i = 0; i < CptForme; i++)
            {
                if (_formes[i].Couleur == couleur)
                {
                    somme += _formes[i].Surface();
                }
            }
            return somme;
        }
    }

}

