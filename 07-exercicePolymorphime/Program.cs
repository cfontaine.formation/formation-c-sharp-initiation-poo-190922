﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePolymorphime
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Terrain terrain = new Terrain();
            terrain.Ajouter(new Cercle(Couleurs.ROUGE,1.0)); ;
            terrain.Ajouter(new Cercle(Couleurs.ROUGE, 1.0));
            terrain.Ajouter(new Rectangle(Couleurs.BLEU,2.0, 1.0 ));
            terrain.Ajouter(new Rectangle(Couleurs.BLEU,2.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleurs.VERT,1.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleurs.VERT,1.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleurs.VERT,1.0, 1.0));
            terrain.Ajouter(new Rectangle(Couleurs.ORANGE,1.0, 1.0));
            terrain.Ajouter(new Rectangle(Couleurs.ORANGE, 1.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleurs.ORANGE, 1.0, 1.0));
            Console.WriteLine($"Surface total= {terrain.Surface()}");
            Console.WriteLine($"Surface Vert= {terrain.Surface(Couleurs.VERT)}");
            Console.WriteLine($"Surface Bleu= {terrain.Surface(Couleurs.BLEU)}");
            Console.WriteLine($"Surface Rouge= {terrain.Surface(Couleurs.ROUGE)}");
            Console.WriteLine($"Surface Orange= {terrain.Surface(Couleurs.ORANGE)}");
            Console.ReadKey();
        }
    }
}
