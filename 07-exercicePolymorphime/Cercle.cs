﻿using System;

namespace _07_exercicePolymorphime
{
    internal class Cercle : Forme
    {
        public double Rayon { get; set; }
        public Cercle(Couleurs couleur, double rayon) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double Surface()
        {
            return Math.PI * Rayon * Rayon;
        }

        public override string ToString()
        {
            return $"Cercle rayon={Rayon} {base.ToString()}";
        }
    }
}
