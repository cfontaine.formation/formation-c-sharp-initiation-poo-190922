﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePolymorphime
{
    enum Couleurs { VERT,BLEU,ROUGE,ORANGE} 
    internal abstract class Forme
    {
        public Couleurs Couleur { get; set; }

        protected Forme(Couleurs couleur)
        {
            Couleur = couleur;
        }

        public abstract double Surface();


        public override string ToString()
        {
            return Couleur.ToString();
        }
    }
}
