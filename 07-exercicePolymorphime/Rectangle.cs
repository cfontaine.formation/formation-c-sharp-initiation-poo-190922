﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePolymorphime
{
    internal class Rectangle : Forme
    {
        public double Largeur { get; set; }

        public double Longueur { get; set; }

        public Rectangle(Couleurs couleur,double largeur, double longueur) : base(couleur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }

        public override double Surface()
        {
            return Largeur * Longueur;
        }

        public override string ToString()
        {
            return $"Rectangle [{Largeur} {Longueur} {base.ToString()}]";
        }
    }
}
