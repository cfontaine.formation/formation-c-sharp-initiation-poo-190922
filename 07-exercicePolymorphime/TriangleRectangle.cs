﻿namespace _07_exercicePolymorphime
{
    internal class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(Couleurs couleur, double largeur, double longueur) : base(couleur, largeur, longueur)
        {
        }

        public override double Surface()
        {
            return base.Surface() / 2.0;
        }

        public override string ToString()
        {
            return $"Triangle {base.ToString()}";
        }
    }
}
