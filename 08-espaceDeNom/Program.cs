﻿
using _08_espaceDeNom.gui;
//using System;


// Définir un Alias
using ConsoleSys = System.Console;

namespace _08_espaceDeNom
{
    internal class Program
    {
        static void Main(string[] args)
        {
            _08_espaceDeNom.gui.Window win = new _08_espaceDeNom.gui.Window();
            Window win2 = new Window();
            _08_espaceDeNom.gui.Console c = new _08_espaceDeNom.gui.Console();
            System.Console.WriteLine();

            Console cnx = new Console();
            ConsoleSys.WriteLine("Hello");
        }
    }
}
