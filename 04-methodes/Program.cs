﻿using System;

namespace _04_methodes
{
    // Une enumération est un ensemble de constante
    enum Motorisation { ESSENCE, DIESEL, ELECTRIQUE, GPL }

    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, OUEST = 180, SUD = 270, EST = 0 }

    // Énumération comme indicateurs binaires
    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE //96
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    };
    internal class Program
    {
        static int Main(string[] args)
        {
            // Appel de methode
            double r = Multiplier(1.3, 6.7);
            Console.WriteLine(r);
            Console.WriteLine(Multiplier(1.4, 1.3));

            // Appel de methode (sans type retour)
            Afficher(3);

            // Exercice Maximum
            Console.WriteLine("Maximum: entrer 2 en");
            int v1 = Convert.ToInt32(Console.ReadLine());
            int v2 = Convert.ToInt32(Console.ReadLine());
            int res = Maximum(v1, v2);
            Console.WriteLine(res);

            // Exercice Paire
            Console.WriteLine(Even(2));
            Console.WriteLine(Even(3));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v = 42;
            TestParamValeur(v);
            Console.WriteLine(v); // 42

            // Passage de paramètre par référence
            //La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref v);
            Console.WriteLine(v); // 23

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int r1,r2;
            // On peut déclarer les variables de retours dans les arguments pendant l'appel de la méthode
            TestParamSortie(out int r1, out int r2);

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(out _, out int r3);
            Console.WriteLine($"{r1} {r2}");

            // Paramètre optionnel
            TestParamOptionnel(2, "world", true);   // 2 world true
            TestParamOptionnel(3);                  // 3 hello false
            TestParamOptionnel(4, "Bonjour");       // 4 Bonjour false

            // Paramètres nommés
            TestParamOptionnel(5, b: true);
            TestParamOptionnel(s: "salut", b: false, i: 6);

            // Exercice Echange
            int val1 = 3;
            int val2 = 7;
            Console.WriteLine($" val1={val1} val2={val2}");
            Swap(ref val1, ref val2);
            Console.WriteLine($" val1={val1} val2={val2}");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(1));
            Console.WriteLine(Moyenne(4, 5, 2));
            Console.WriteLine(Moyenne(4, 5, 2, 5, 6, 2, 6, 6));
            int[] valeur = { 3, 5, 2, 6, 2, 8, 5 };
            Console.WriteLine(Moyenne(4, valeur));  // Passage d'un tableau en paramètre

            // Surcharge de méthode
            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 2));     // => appel de la méthode avec 2 int en paramètres
            Console.WriteLine(Somme(1.4, 2.5)); // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme(4.5, 10));  // => appel de la méthode avec 1 double et 1 entier en paramètre
            Console.WriteLine(Somme("Hello", " world")); // => appel de la méthode avec 2 chaines en paramètre

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(2L, 2));  // => appel de la méthode avec 1 double et 1 entier en paramètre
            Console.WriteLine(Somme(2L, 5L)); // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme(1, 5.6)); // => appel de la méthode avec 2 double en paramètres

            // Pas de conversion possible => Erreur
            // Console.WriteLine(Somme(1.4M, 5.6M));

            // Exercice Tableau
            Menu();

            // Méthode récursive
            int fact = Factorial(3);
            Console.WriteLine($"factoriel de 3={fact}");

            // Paramètre de la méthode Main
            // En lignes de commandes: 05 - methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet->onglet Déboguer->options de démarrage -> Arguments de la ligne de commande
            foreach (var s in args)
            {
                Console.WriteLine(s);
            }

            // Corps de méthode
            Console.WriteLine(Soustration(1, 2));
            AfficherResultat(6);

            #region Enumération
            // m est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation m = Motorisation.ESSENCE;

            Console.WriteLine(m); // ESSENCE

            // Enum -> string
            string mStr = m.ToString();
            Console.WriteLine(mStr);  // ESSENCE

            // enum -> entier (cast)
            int iM = (int)m;
            Console.WriteLine(iM);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation m2 = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL");
            Console.WriteLine(m2);

            // m2 = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2");
            // Console.WriteLine(m);

            // entier -> enum
            int vm = 2;
            if (Enum.IsDefined(typeof(Motorisation), vm))   // Permet de tester si la valeur entière existe dans l'enumération
            {
                Motorisation m3 = (Motorisation)vm;
                Console.WriteLine(m3);
            }

            // Énumération comme indicateurs binaires
            JourSemaine jourSemaine = JourSemaine.LUNDI | JourSemaine.MERCREDI;

            // Avec l'attribut [Flag] -> affiche LUNDI, MERCREDI , sans -> affiche 5
            Console.WriteLine(jourSemaine);
            if ((jourSemaine & JourSemaine.LUNDI) != 0) // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            if ((jourSemaine & JourSemaine.MARDI) != 0) // teste la présence de MARDI
            {
                Console.WriteLine("Mardi");
            }
            jourSemaine |= JourSemaine.SAMEDI;
            if ((jourSemaine & JourSemaine.WEEKEND) != 0)
            {
                Console.WriteLine("Week-end");
            }
            Console.WriteLine(jourSemaine);

            // Utilisation d'une énumération avec switch
            switch (m)
            {
                case Motorisation.ESSENCE:
                    Console.WriteLine("Essence");
                    break;
                case Motorisation.DIESEL:
                    Console.WriteLine("Diesel");
                    break;
                default:
                    Console.WriteLine("Autre motorisation");
                    break;
            }
            #endregion

            #region Structure
            Point p1;
            p1.X = 3;   // accès au champs x de la structure
            p1.Y = 4;
            Console.WriteLine($"X={p1.X}  Y={p1.Y}");
            Console.WriteLine(p1);

            // Affectation d'une structure
            Point p2 = p1;
            p2.X = 8;
            Console.WriteLine($"X={p2.X}  Y={p2.Y}"); // Les champs de p2 sont initialisé avec les valeurs des champs de p1

            // Comparaison de 2 structures => Il faut comparer chaque champs
            if (p1.X == p2.X && p1.Y == p2.Y)
            {
                Console.WriteLine("p1  est égal à p2");
            }
            #endregion
            Console.ReadKey();
            return 0;
        }

        // Déclaration
        static double Multiplier(double v1, double v2)
        {
            return v1 * v2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(int i) // void => pas de valeur retourné
        {
            Console.WriteLine(i);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int a, int b)
        {
            return a > b ? a : b;

            // ou

            //if (a > b)
            //{
            //    return a;
            //}
            //else
            //{
            //    return b;
            //}    
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Even(int val)
        {
            return val % 2 == 0;
            // ou
            //if (val % 2 == 0) // ou ((val&1) ==0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        #endregion

        #region Passage de paramètre
        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 23; // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a = 23;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamSortie(out int a, out int b)
        {
            // Console.WriteLine(a); // erreur, on ne peut pas accéder en entrée à un paramètre out 
            a = 23;
            Console.WriteLine(a);
            b = a + 1;              // La méthode doit obligatoirement affecter une valeur aux paramètres out
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int i, string s = "hello", bool b = false)
        {
            Console.WriteLine($"i={i} s={s} b={b}");
        }

        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre
        static void Swap(ref int x, ref int y)
        {
            int tmp = x;
            x = y;
            y = tmp;
        }
        #endregion

        // Nombre d'arguments variable => params
        static double Moyenne(int v, params int[] valeurs)
        {
            double somme = v;
            foreach (var elm in valeurs)
            {
                somme += elm;
            }
            return somme / (valeurs.Length + 1);
        }

        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(double d1, double d2)
        {
            Console.WriteLine("2 doubles");
            return d1 + d2;
        }

        static double Somme(double a, int b)
        {
            Console.WriteLine("un double, un entier");
            return a + b;
        }

        //static int Somme(int i1, int i2)
        //{
        //    return i1 + i2;
        //}
        static string Somme(string s1, string s2)
        {
            Console.WriteLine("2 chaine de caractères");
            return s1 + s2;
        }
        #endregion

        #region Exercice Tableau
        // - Écrire une méthode qui affiche un tableau d’entier

        // - Écrire une méthode qui permet de saisir :
        //   - La taille du tableau
        //   - Les éléments du tableau

        // - Écrire une méthode qui calcule :
        //   - le maximum
        //   - la moyenne

        // - Faire un menu qui permet de lancer ces méthodes
        //      1 - Saisir le tableau
        //      2 - Afficher le tableau
        //      3 - Afficher le maximum et la moyenne
        //      0 - Quitter
        //      Choix =
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tabInt = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tabInt[{i}]= ");
                tabInt[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tabInt;
        }

        static void CalculTab(int[] tab, out int maximum, out double moyenne)
        {
            maximum = tab[0];
            double somme = 0.0;
            foreach (var v in tab)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            moyenne = somme / tab.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("\n1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] t = null;
            AfficherMenu();
            int choix;
            do
            {
                Console.Write("choix= ");
                choix = Convert.ToInt32(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        t = SaisirTab();
                        break;
                    case 2:
                        if (t != null)
                        {
                            AfficherTab(t);
                        }
                        break;
                    case 3:
                        if (t != null)
                        {
                            CalculTab(t, out int max, out double moyenne);
                            Console.WriteLine($"Maximum={max} Moyenne={moyenne}");
                        }
                        break;
                    case 0:
                        Console.WriteLine("au revoir !");
                        break;
                    default:
                        AfficherMenu();
                        Console.WriteLine($"Le choix {choix} n'existe pas");
                        break;
                }
            }
            while (choix != 0);
            #endregion
        }

        // Récursivité: une méthode qui s'appelle elle-même
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        // Corps de méthode
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>
        static int Soustration(int x, int y) => x - y;
        //{
        //    return x - y;
        //}

        static void AfficherResultat(int a) => Console.WriteLine(a);
        //{
        //    Console.WriteLine(a);
        //}

    }
}

