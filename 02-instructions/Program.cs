﻿using System;

namespace _02_instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Condition if
            Console.WriteLine("Saisir un nombre entier");
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > 100)
            {
                Console.WriteLine("La saisie est supérieur à 100");
            }
            else if (v < 100)
            {
                Console.WriteLine("La saisie est inférieure 100");
            }
            else
            {
                Console.WriteLine("La saisie est égale à 100");
            }
            #endregion

            #region Exercice: trie de 2 valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.WriteLine("Trie: saisir 2 entiers ");
            int v1 = Convert.ToInt32(Console.ReadLine());
            int v2 = Convert.ToInt32(Console.ReadLine());
            if (v1 < v2)
            {
                Console.WriteLine($"{v1}<{v2}");
            }
            else
            {
                Console.WriteLine($"{v2}<{v1}");
            }
            #endregion

            #region Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.Write("Intervalle: Saisir un entier ");
            int val = Convert.ToInt32(Console.ReadLine());
            if (val > -4 && val <= 7)
            {
                Console.WriteLine($"{val} fait partie de l'interval");
            }
            #endregion

            #region condition switch
            Console.Write("Saisir un jour entre 1 et 7 ");
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            #region calculatrice
            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide:  + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0
            
            Console.WriteLine("Calculatrice: saisir un double, un opérateur + - * / et un double");
            double a = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double b = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{a} + {b} = {a + b}");
                    break;
                case "-":
                    Console.WriteLine($"{a} - {b} = {a - b}");
                    break;
                case "*":
                    Console.WriteLine($"{a} * {b} = {a * b}");
                    break;
                case "/":
                    if (b == 0.0)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{a} / {b} = {a / b}");
                    }
                    break;
                default:
                    Console.WriteLine($"{op} n'est pas un opérateur correct");
                    break;
            }
            #endregion

            #region Opérateur ternaire
            Console.Write("Valeur absolue: Saisir une valeur double ");
            double va = Convert.ToDouble(Console.ReadLine());

            double abs = va < 0.0 ? -va : va;
            
            Console.WriteLine($"|{va}|={abs}");
            #endregion

            #region Boucle while
            int j = 0;
            while (j < 10)
            {
                Console.WriteLine(j);
                j++;
            }
            #endregion

            #region Boucle do while
            j = 0;
            do
            {
                Console.WriteLine(j);
                j++;
            }
            while (j < 10);
            #endregion

            #region  boucle for
            for (int i = 0; i < 10; i++) // i+=2 permet d'incrémenter avec un pas de 2  
            {
                Console.WriteLine(i);
            }
            #endregion

            #region break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    break;  // break => termine la boucle
                }
                Console.WriteLine(i);
            }
            #endregion

            #region continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine(i);
            }
            #endregion

            // goto pour quitter des boucles imbriquées
            for (int i = 0; i < 10; i++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (i == 1)
                    {
                        goto EXIT_LOOP;
                    }
                    Console.WriteLine($"{i} {k}");
                }
            }
        EXIT_LOOP:

            #region Exercice boucle
            //Table de multiplication
            //Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9

            //    1 X 4 = 4
            //    2 X 4 = 8
            //    …
            //    9 x 4 = 36

            //Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur

            // Version 1
            int m;
            Console.WriteLine("Saisir un entier entre 1 et 9 pour afficher sa table de multiplication");
            do
            {
                m = Convert.ToInt32(Console.ReadLine());
                if (m >= 1 && m <= 9)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        Console.WriteLine($"{i} x {m} = {i * m}");
                    }
                }
            } while (m >= 1 && m <= 9);

            Console.WriteLine("Saisir un entier entre 1 et 9 pour afficher sa table de multiplication");
            // Version 2: avec une boucle infinie et un break
            for (; ; ) // while(true) -> boucle infinie
            {
                int mul = Convert.ToInt32(Console.ReadLine());
                if (mul < 1 || mul > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{i} x {mul} = {i * mul}");
                }
            }

            // Quadrillage 
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //    ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]
            Console.WriteLine("Quadrillage");
            Console.Write("Nombre de colonne: ");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre de ligne: ");
            int row = Convert.ToInt32(Console.ReadLine());
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[]");
                }
                Console.WriteLine();
            }
            #endregion

            #region Complément switch
            // Clause when (C# 7.0) => Ajouter une condition supplémentaire sur un case
            int jour = 6;
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case var x when x == 6 || x == 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // goto avec un switch
            jour = 1;
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    //goto case 7;
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            Console.ReadKey();
        }
    }
}
