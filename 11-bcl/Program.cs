﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _11_bcl
{ }
internal class Program
{
    static void Main(string[] args)
    {
        #region Chaine de caractères
        string hello = "Hello"; // littéral
        Console.WriteLine(hello);
        string str = new string('a', 10);
        Console.WriteLine(str);
        Console.WriteLine("Bonjour".ToUpper());

        // Length -> Nombre de caractère de la chaine de caractère
        Console.WriteLine(str.Length); //10

        // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
        Console.WriteLine(hello[1]);

        // Concaténation 
        // avec l'opérateur + 
        string str2 = hello + " world";
        Console.WriteLine(str2);
        // ou avec la méthode de classe Concat
        string str3 = string.Concat(hello, " world");
        Console.WriteLine(str3);

        // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
        string str4 = string.Join(";", "azerty", "rtyu", "uiop", "dfghj", "cvbn");
        Console.WriteLine(str4);

        // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
        string[] tabS = str4.Split(';');
        foreach (var e in tabS)
        {
            Console.WriteLine(e);
        }

        // Substring permet d'extraire une sous-chaine d'une chaine
        // de l'indice passé en paramètre juqu'à la fin de la chaine
        Console.WriteLine(str3.Substring(6));
        // ou pour le nombre de caractères passé en paramètre
        Console.WriteLine(str3.Substring(6, 2));

        // Insérer une sous-chaine à partir d'un indice
        Console.WriteLine(str3.Insert(5, "------------"));

        // Supprimer un nombre de caractère à partir d'un indice
        Console.WriteLine(str3.Remove(5, 4));

        // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
        Console.WriteLine(str3.StartsWith("Hell"));
        Console.WriteLine(str3.StartsWith("aaa"));

        // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
        Console.WriteLine(str3.IndexOf("o")); // 4
        Console.WriteLine(str3.IndexOf("o", 5)); // 7   idem mais à partir de l'indice passé en paramètre
        Console.WriteLine(str3.IndexOf("o", 8)); // -1  -1 retourne -1, si le caractère n'est pas trouvé

        // Remplace toutes les chaines (ou caratères) oldValue par newValue
        Console.WriteLine(str3.Replace('o', 'a'));

        // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
        Console.WriteLine(str3.Contains("wo"));
        Console.WriteLine(str3.Contains("aaa"));

        // Trim supprime les caractères de blanc du début et de la fin de la chaine
        string str5 = "     \n \t   azerrty yuiop \n \n \t";
        Console.WriteLine(str5);
        Console.WriteLine(str5.Trim());
        Console.WriteLine(str5.TrimStart());    // idem mais uniquement au début de la chaine
        Console.WriteLine(str5.TrimEnd());      // idem mais uniquement à la fin de la chaine
        Console.WriteLine(str3.ToLower());

        // Aligne les caractères à gauche en les complétant par un caractère (par défaut ' ') à gauche pour une longueur spécifiée
        Console.WriteLine(str3.PadLeft(40));
        Console.WriteLine(str3.PadLeft(40, '_'));

        // Aligne les caractères à droite en les complétant par un caractère (par défaut ' ') à droite pour une longueur spécifiée
        Console.WriteLine(str3.PadRight(40, '_'));
        Console.WriteLine(str3.PadRight(5, '_'));

        // égalité de 2 chaines  == ou equals
        Console.WriteLine("hello" == hello);        // L'opérateur == est surchargé dans la classe string
        Console.WriteLine("hello".Equals(hello));

        // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
        // Il existe 2 méthodes: une d'instance et de classe
        Console.WriteLine(string.Compare(hello, "bonjour"));//1
        Console.WriteLine(hello.CompareTo("bonjour")); //-1

        // On peut chainer l'appel des différentes méthodes
        Console.WriteLine(str3.ToUpper().Substring(4).Trim());

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("test");
        sb.Append("____");
        sb.Append(123);
        sb.Remove(4, 2);
        string str7 = sb.ToString();
        Console.WriteLine(str7);

        // Inverser
        Console.WriteLine(Inverser("Bonjour"));

        // Palindrome
        Console.WriteLine(Palindrome("Radar"));
        Console.WriteLine(Palindrome("Bonjour"));
        #endregion

        #region DateTime
        DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
        Console.WriteLine(d);

        DateTime dUtc = DateTime.UtcNow;
        Console.WriteLine(dUtc);

        Console.WriteLine(d.Year);
        Console.WriteLine(d.DayOfYear);

        DateTime finAnnee = new DateTime(2022, 12, 31);
        Console.WriteLine(finAnnee);

        // TimeSpan => représente une durée
        TimeSpan duree = finAnnee - d;
        Console.WriteLine(duree);
        Console.WriteLine(duree.TotalHours);

        TimeSpan dixJour = new TimeSpan(10, 0, 0, 0);
        Console.WriteLine(dixJour);
        Console.WriteLine(d + dixJour);

        // Comparaison
        Console.WriteLine(DateTime.Compare(d, finAnnee)); // -1
        Console.WriteLine(finAnnee.CompareTo(d)); // 1
        Console.WriteLine(d < finAnnee); // true
        Console.WriteLine(d == finAnnee); // false

        // DateTime -> string
        Console.WriteLine(d.ToLongDateString());
        Console.WriteLine(d.ToShortDateString());
        Console.WriteLine(d.ToLongTimeString());
        Console.WriteLine(d.ToShortTimeString());

        Console.WriteLine(d.ToString("ddd MM yyyy"));

        // string -> DateTime
        DateTime d2 = DateTime.Parse("2022/10/01T10:45:00");
        Console.WriteLine(d2);
        #endregion

        #region Collection
        // Collection faiblement typé (C# 1) => elle peut contenir tous types d'objets
        ArrayList lst = new ArrayList();
        lst.Add("Hello");
        lst.Add(12); // Boxing: object <- type simple 
        lst.Add('a');
        //int i = (int)lst[1]; // UnBoxing:  type simple <-object
        Console.WriteLine(lst[0]);
        if (lst[0] is string str8)
        {
            Console.WriteLine(str8);
        }

        // Collection fortement typée (à partir de c#2) => type générique
        List<string> lstStr = new List<string>();
        lstStr.Add("Hello");
        lstStr.Add("Bonjour");
        lstStr.Add("World");
        lstStr.Add("a suprimer 1");
        lstStr.Add("a suprimer 2");
        //lstStr.Add(12);               // On ne peut plus qu'ajouter des chaines de caractères => sinon erreur de complilation

        Console.WriteLine(lstStr[0]);   // Accès à un élément de la liste
        lstStr[0] = "Salut";

        Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
        Console.WriteLine(lstStr.Max());    // Valeur maximum stocké dans la liste
        Console.WriteLine(lstStr.Min());    // Inverser l'ordre de la liste

        lstStr.Remove("a suprimer 2");      // supprimer la chaine de la liste
        lstStr.RemoveAt(3);                 // supprimer le 4ème élément de la liste
        Console.WriteLine("-----------");

        // Parcourir la collection complétement (à partir de c#2)
        foreach (var s in lstStr)
        {
            Console.WriteLine(s);
        }

        lstStr.Reverse();   // Inverser l'ordre de la liste

        // Parcourir un collection avec un Enumérateur
        var it = lstStr.GetEnumerator();
        while (it.MoveNext())
        {
            Console.WriteLine(it.Current);
        }

        // IEnumerable
        foreach (var i in NombrePaire())
        {
            Console.WriteLine(i);
        }

        // IEnumerable -> Parcours d'une chaine
        string str9 = "azerty";
        foreach (var c in str9)
        {
            Console.WriteLine(c);
        }

        // Dictionnary => Clé / Valeur
        Dictionary<int, string> m = new Dictionary<int, string>();
        m.Add(123, "azerty");        // Add => ajout d'un valeur associé à une clé
        m.Add(12, "hello");
        m.Add(345, "World");
        m.Add(56, "a supprimer");
        //m.Add(56, "azeaaze");      // => On ne peut pas ajouter, si la clé éxiste déjà => exception

        // accès à un élément m[clé] => valeur
        Console.WriteLine(m[123]);  // azerty
        m[123] = "Bonjour";         // modifier une valeur associé à la clé
        m.Remove(56);

        Console.WriteLine(m.Count);   // Nombre d'élément du Dictionnary
        //m.Clear();                  // éffacer tous les éléments du dictionnary

        // Parcourir un dictionnary
        foreach (var kv in m)
        {
            Console.WriteLine($"{kv.Key} {kv.Value}");
        }

        // Contains
        Console.WriteLine(m.ContainsKey(123));          //-> tester l'existance d'une clé
        Console.WriteLine(m.ContainsValue("hello"));    //-> tester l'existance d'un valeur

        // Obtenir et parcourir toutes les clés du dictionary
        var keys = m.Keys;
        foreach (var k in keys)
        {
            Console.WriteLine(k);
        }

        // Obtenir et parcourir toutes les valeurs du dictionary
        var values = m.Values;
        foreach (var v in values)
        {
            Console.WriteLine(v);
        }

        // Stack =>FILO
        Stack<int> st = new Stack<int>();
        st.Push(12);      // Ajouter un élément
        st.Push(123);
        st.Push(456);

        foreach (var j in st)
        {
            Console.WriteLine(j);
        }

        Console.WriteLine(st.Count);    // Nombre d'élément dans la pile   -> 3
        Console.WriteLine(st.Peek());   // Peek => Lire l'élément en tête de la pile sans le retirer  456
        Console.WriteLine(st.Peek());   // 456

        Console.WriteLine(st.Pop());    // Pop => Lire l'élément en tête de la pile et le retire de la pile 456
        Console.WriteLine(st.Pop());    //123
        #endregion
        Console.ReadKey();
    }

    // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
    // exemple : bonjour => ruojnob
    public static string Inverser(string s)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = s.Length - 1; i >= 0; i--)
        {
            sb.Append(s[i]);
        }
        return sb.ToString();
    }


    // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
    // exemple :: SOS, radar
    public static bool Palindrome(string str)
    {
        string tmp = str.ToLower().Trim();
        return tmp == Inverser(tmp);
    }

    public static IEnumerable<int> NombrePaire()
    {
        for (int i = 0; i < 100; i += 2)
        {
            yield return i;
        }
    }
}


