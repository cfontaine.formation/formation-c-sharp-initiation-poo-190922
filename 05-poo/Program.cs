﻿using System;

namespace _05_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel d'une méthode de classe
            Voiture.TestMethodeDeClasse();

            // Accès à une variable de classe
            Console.WriteLine($"nombre de voiture={Voiture.NbVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            Console.WriteLine($"nombre de voiture={Voiture.NbVoiture}");

            // Accès à une variable d'instance
            //v1.Vitesse=10;//v1.vitesse = 10; //SetVitesse(10)
            Console.WriteLine(v1.Vitesse); //v1.vitesse //v1.GetVitesse()

            // Appel d’une méthode d’instance
            v1.Accelerer(25);
            v1.Afficher();
            v1.Freiner(15);
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());


            Voiture v2 = new Voiture();
            Console.WriteLine($"nombre de voiture={Voiture.NbVoiture}");
            //v2._vitesse = 30;
            Console.WriteLine(v2.Vitesse);
            v2 = null;  // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                        // Il sera détruit lors de la prochaine execution du garbage collector
                        // appel explicite du garbage collector (à éviter)
                        //   GC.Collect();
            Voiture v3 = new Voiture("Honda", "Noir", "az-123-FR");
            Console.WriteLine($"nombre de voiture={Voiture.NbVoiture}");
            v3.Afficher();

            // Initialiseur d'objet
            // Avec un initialiseur d'objet on a accès uniquement à ce qui est public
            //Voiture v5 = new Voiture { couleur = "Jaune", _marque = "Fiat", plaqueIma = "ze-345-fr", _vitesse = 40, compteurKm = 100 };
            //Console.WriteLine($"nombre de voiture={Voiture.nbVoiture}");
            //v5.Afficher();

            Console.WriteLine(Voiture.EgalVitesse(v1, v3));

            #region Classe statique
            //  Math m = new Math();    // ne peut pas être instanciée
            // ne peut pas contenir de constructeurs d’instances
            Console.WriteLine(Math.Abs(-5));    // contient uniquement des membres statiques
            #endregion

            #region Agrégation
            Personne p = new Personne("Doe", "John");
            Voiture v6 = new Voiture("Ford", "bleu", "wx-3456-Fr", p);
            v6.Afficher();
            v1.Proprietaire = p;
            v1.Afficher();
            #endregion

            #region Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Afficher();
            vp1.Accelerer(20);
            vp1.Afficher();
            vp1.AllumerGyro();
            Console.WriteLine($"Gyro={vp1.Gyro}");
            #endregion

            #region Redéfinition Occulttion
            VoiturePrioritaire vp2 = new VoiturePrioritaire("toyota", "blanc", "df-457-rf", true);
            vp2.Afficher();
            #endregion

            #region Exercice CompteBancaire
            CompteBancaire cb1 = new CompteBancaire(100.0, p);
            // cb1.solde = 100.0;
            // cb1.iban = "fr-596200000";
            // cb1.titulaire = "John Doe";

            // Console.WriteLine(cb1.solde);
            cb1.Crediter(50.0);
            cb1.Afficher();
            cb1.Debiter(300.0);
            Console.WriteLine(cb1.EstPositif());

            Personne p2 = new Personne("Doe", "Jane");
            CompteBancaire cb2 = new CompteBancaire(p2);
            cb2.Afficher();

            CompteEpargne ce1 = new CompteEpargne(p2, 1.5);
            ce1.Afficher();
            ce1.CalculInteret();
            ce1.Afficher();
            #endregion

            #region Exercice Point
            Point a = new Point();
            a.Afficher();
            a.Deplacer(2, 3);
            a.Afficher();
            Console.WriteLine(a.Norme());

            Point b = new Point(1, 3);
            b.Afficher();
            Console.WriteLine(Point.Distance(a, b));
            #endregion

            Console.ReadKey();
        }
    }
}
