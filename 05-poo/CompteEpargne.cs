﻿namespace _05_poo
{
    internal class CompteEpargne : CompteBancaire
    {
        public double Taux { get; set; }

        public CompteEpargne(double taux) //:base() fait implicitement
        {
            Taux = taux;
        }

        public CompteEpargne(Personne titulaire, double taux) : base(titulaire)
        {
            Taux = taux;
        }

        public void CalculInteret()
        {
            Solde = Solde * (1 + Taux / 100);
        }
    }
}
