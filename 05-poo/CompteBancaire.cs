﻿using System;

namespace _05_poo
{
    internal class CompteBancaire
    {
        public double Solde { get; protected set; } = 50.0;

        public string Iban { get; }

        public Personne Titulaire { get; set; }

        private static int _nombreCompte;

        public CompteBancaire()
        {
            _nombreCompte++;
            Iban = "fr-5962-0000-" + _nombreCompte;
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("------------------------");
            Console.WriteLine($"Solde={Solde}");
            Console.WriteLine($"Iban={Iban}");
            Console.WriteLine($"Titulaire=");
            Titulaire?.Afficher();
            Console.WriteLine("------------------------");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0)
            {
                Solde += valeur;
            }
        }
        public void Debiter(double valeur)
        {
            if (Solde > 0)
            {
                Solde -= valeur;
            }
        }
        public bool EstPositif()
        {
            return Solde >= 0;
        }
    }
}
