﻿using System;

namespace _05_poo
{
    internal class Personne
    {
        public string Nom { get; set; }

        public string Prenom { get; set; }

        public Personne(string nom, string prenom)
        {
            Nom = nom;
            Prenom = prenom;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom}");
        }
    }
}
