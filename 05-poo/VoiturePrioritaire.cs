﻿using System;

namespace _05_poo
{
    internal class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {
        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire() //:base() -> implicite
        {
            Console.WriteLine("constructeur par défaut voiture prioritaire");
        }

        public VoiturePrioritaire(bool gyro) //:base()
        {
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma, bool gyro) : base(marque, couleur, plaqueIma)
        {
            Console.WriteLine("constructeur 3 param voiture prioritaire");
            Gyro = gyro;
        }

        public bool Gyro { get; set; }

        public void AllumerGyro()
        {
            Vitesse += 20;
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        // Occultation => redéfinir une méthode d'une classe mère et  « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            Vitesse += 2 * vAcc;
        }

        // Redéfinition
        public override void Afficher()
        {
            base.Afficher();    // base => pour appeler une méthode de la classe mère
            Console.WriteLine($"Gyro={Gyro}");
        }

    }
}
