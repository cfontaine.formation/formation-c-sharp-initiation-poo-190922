﻿using System;

namespace _05_poo
{
    internal /*sealed*/ class Voiture
    {
        // Variables d'instances => Etat
        private string _marque;

        // On n'a plus besoin de déclarer les variables d'instances _couleur,_compteurKm, _plaqueIma, elles seront générées automatiquement par les propriétées automatique
        // private int _vitesse;
        // private string couleur = "Rouge";
        // private string plaqueIma;
        // private int compteurKm = 20;

        // Propriété
        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string PlaqueIma { get; }
        public string Couleur { get; set; } = "Rouge";
        public int CompteurKm { get; } = 20;    // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        public int Vitesse { get; protected set; }
        public Personne Proprietaire { get; set; }

        // Variable de classe
        // private static int nbVoiture; // Remplacer par une propriétée static
        public static int NbVoiture { get; private set; }

        //  C# => propriété
        public string Marque
        {
            get => _marque; // en C# 7.0
            //{
            //    return marque;
            //}
        }

        //public int Vitesse
        //{
        //    get // accès en lecture
        //    {
        //        return _vitesse;
        //    }
        //    set
        //    {
        //        if (value > 0)
        //        {
        //            _vitesse = value;
        //        }
        //    }
        //}

        // Constructeurs

        // Constructeur par défaut
        public Voiture()
        {
            NbVoiture++;
            Console.WriteLine("Constructeur par défaut");
        }

        // : this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres");
            this._marque = marque;
            Couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        // : this(marque, couleur, plaqueIma) => Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 4 paramètres");
            Vitesse = vitesse;
        }

        public Voiture(string marque, string plaqueIma, string couleur, Personne proprietaire) : this(marque, plaqueIma, couleur)
        {
            Proprietaire = proprietaire;
        }

        //Destructeur
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur Voiture");
        //}

        // constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static");
        }

        // en Java  en C++
        // getter => accès en lecture
        //public int GetVitesse()
        //{
        //    return vitesse;
        //}

        // Setter => accès en écriture
        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse > 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //}

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
            if (Vitesse < 0)
            {
                Vitesse = 0;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            if (Vitesse == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            //   return Vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"Voiture: {_marque} {Couleur} {PlaqueIma} {Vitesse} {CompteurKm}");
            //if (Proprietaire != null)
            //{
            //    Proprietaire.Afficher();
            //}
            Proprietaire?.Afficher();
        }

        // Méthode de classe
        public static void TestMethodeDeClasse()
        {
            Console.WriteLine("Méthode de classe");
            Console.WriteLine(NbVoiture);
            //Console.WriteLine(vitesse);  // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            //Accelerer();                 // ou une méthode d'instance
        }

        public static bool EgalVitesse(Voiture vA, Voiture vB)
        {
            return vA.Vitesse == vB.Vitesse; // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
