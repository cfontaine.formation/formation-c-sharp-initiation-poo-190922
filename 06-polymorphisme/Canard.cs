﻿using System;

namespace _06_polymorphisme
{
    internal class Canard : Animal,IPeutMarcher, IPeutVoler
    {
        public Canard(int age, int poid) : base(age, poid)
        {
        }


        public override void EmettreSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le carnard Marche");
        }

        public void Courrir()
        {
            Console.WriteLine($"Le carnard Court");
        }

        public void Decoller()
        {
            Console.WriteLine($"Le carnard décole");
        }

        public void Aterrir()
        {
            Console.WriteLine($"Le carnard aterrit");
        }
    }
}
