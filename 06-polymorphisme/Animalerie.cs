﻿namespace _06_polymorphisme
{
    internal class Animalerie
    {
        public Animal[] Place { get; set; } = new Animal[20];

        private int placeOccupe;

        public void Ajouter(Animal a)
        {
            if (placeOccupe < Place.Length)
            {
                Place[placeOccupe] = a;
                placeOccupe++;
            }
        }

        public void Ecouter()
        {
            for (int i = 0; i < placeOccupe; i++)
            {
                Place[i].EmettreSon();
            }
        }

    }
}
