﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_polymorphisme
{
    internal class Labrador : Chien
    {
        public Labrador(int age, int poid, string nom) : base(age, poid, nom)
        {
        }

        // Comme la méthode est sealed dans la classe mère, on ne peut plus redéfinir la méthode
        //public override void EmettreSon()
        //{
        //    Console.WriteLine("Il parle");
        //}
    }
}
