﻿using System;

namespace _06_polymorphisme
{
    internal class Chat : Animal,IPeutMarcher
    {
        public int NombreVie { get; set; } = 9;

        public Chat(int age, int poid, int nombreVie) : base(age, poid)
        {
            NombreVie = nombreVie;
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Le chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le chat Marche");
        }

        public void Courrir()
        {
            Console.WriteLine($"Le chat Court");
        }
    }
}
