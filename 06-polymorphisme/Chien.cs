﻿using System;
using System.Collections.Generic;

namespace _06_polymorphisme
{
    internal class Chien : Animal , IPeutMarcher  // La classe Chien hérite de la classe Animal
    {
        public string Nom { get; set; }
        public Chien(int age, int poid, string nom) : base(age, poid)
        {
            Nom = nom;
        }

        public sealed override void EmettreSon()    // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal
        {                                           // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes
            Console.WriteLine($"{Nom} aboie");
        }

        public override string ToString()
        {
            return $"Chien[{base.ToString()},{Nom}]";
        }

        public void Marcher()
        {
            Console.WriteLine($"{Nom} Marche");
        }

        public void Courrir()
        {
            Console.WriteLine($"{Nom} Court");
        }

        public override bool Equals(object obj)
        {
            return obj is Chien chien &&
                   base.Equals(obj) &&
                   Age == chien.Age &&
                   Poid == chien.Poid &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            int hashCode = 1240350572;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            return hashCode;
        }
    }
}
