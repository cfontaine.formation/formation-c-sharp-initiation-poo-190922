﻿using System;

namespace _06_polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Polymorphisme
            //Animal a = new Animal(3, 4000);   // Impossible la classe est abstraite
            //a.EmettreSon();

            Chien ch1 = new Chien(4, 3500, "Laika");
            ch1.EmettreSon();

            // upcasting
            Animal a2 = new Chien(3, 2000, "Idefix"); // On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                      // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
                                                      // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée
            a2.EmettreSon();

            //downcasting
            if (a2 is Chien)     // test si a2 est de "type" Chien
            {
                // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
                //Chien ch2 = (Chien)a2; 
                Chien ch2 = a2 as Chien; // as equivalant à un cast pour un objet
                Console.WriteLine(ch2.Nom); // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            }
            if (a2 is Chien ch3) // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                Console.WriteLine(ch3.Nom);
            }

            Animalerie an = new Animalerie();
            an.Ajouter(new Chien(3, 7000, "Rolo"));
            an.Ajouter(new Chien(4, 3500, "Laika"));
            an.Ajouter(new Chat(3, 4000, 7));

            an.Ecouter();
            #endregion

            #region Object
            // Object

            // toString
            // Console.WriteLine(ch1.ToString());
            Console.WriteLine(ch1); // ToString appeler implicitement

            // Equals et getHash
            Chien ch5 = new Chien(4, 3500, "Laika");
            Chien ch6 = new Chien(4, 3500, "Laika");

            Console.WriteLine(ch5 == ch6); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents -> false
            Console.WriteLine(ch5.Equals(ch6)); // si Equal a été redéfinie -> true
            #endregion

            // Interface
            IPeutMarcher ip = new Chien(5, 3000, "Tom");
            ip.Marcher();
            ip.Courrir();
            Console.ReadKey();
        }
    }
}
