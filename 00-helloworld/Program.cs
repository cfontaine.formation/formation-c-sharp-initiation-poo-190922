﻿using System;

namespace _00_helloworld
{
    /// <summary>
    /// La classe Program
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Point d'entrée du programme
        /// </summary>
        /// <param name="args">Arguments ligne de commande</param>
        static void Main(string[] args) // commentaire sur une seule ligne
        {
            /* Commentaire
             * sur 
             * plusieurs lignes*/
            Console.WriteLine("Hello World!!");

            Console.ReadKey();
        }
    }
}
