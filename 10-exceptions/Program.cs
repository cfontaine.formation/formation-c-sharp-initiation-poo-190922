﻿using System;

namespace _10_exceptions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] tb = new int[4];
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(tb[index]);                       // Peut générer une exception IndexOutOfRangeException , si index est > à Length
                int age = Convert.ToInt32(Console.ReadLine());      // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                Console.WriteLine("Suite du programme");
            }
            catch (IndexOutOfRangeException e)  // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("En dehors des limites du tableau");
                Console.WriteLine(e.Message);   // Message -> permet de récupérer le messsage de l'exception
            }
            catch (FormatException e)           // Attrape les exceptions FormatException
            {
                Console.WriteLine("Erreur de format");
                Console.WriteLine(e.StackTrace);    // StackTrace -> 
            }
            catch (Exception e)                 // Attrape tous les autres exception
            {
                Console.WriteLine("Autre exception");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally      // Le bloc finally est toujours éxécuté
            {
                // finally -> généralement utilisé pour libérer les ressources
                Console.WriteLine("Toujours executé");
            }
            Console.WriteLine("Fin du programme");
            Console.ReadKey();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début traitement employé");
            // Traitement partiel de l'exception AgeNegatifException
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age < -10)    // when -> permet d'ajouter une condition si elle est vrai le bloc catch est executé 
            {                                                   // age < -10 par exemple
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"TraitementAge Local de l'exception age={e.Age}  -10");
                throw;  // On relance l'exception pour que l'utilisateur de la méthode la traite à son niveau   
            }
            catch (AgeNegatifException e) when (e.Age >= -10)
            {
                Console.WriteLine(e.Message);
                throw new ArgumentException("Traitement employé age négatif", e);   // On relance une exception différente
            }                                                                       // e => référence à l'exception qui a provoquer l'exception
            Console.WriteLine("Fin traitement employé");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début traitement age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, $"Age négatif: {age}");  // throw => Lancer un exception
                                                                            //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
            }                                                               //  si elle n'est pas traiter, aprés la méthode Main => arrét du programme
            Console.WriteLine("Fin traitement age");
        }
    }
}
